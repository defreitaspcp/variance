##Variância
A variância de uma variável aleatória ou processo estocástico é uma medida da sua dispersão estatística, indicando "o quão longe" em geral os seus valores se encontram do valor esperado.

##Variância Amostral
A variância amostral é uma medida de dispersão ou variabilidade dos dados, relativamente à medida de localização média.

##Variância Populacional
Variância populacional de uma variável de tipo quantitativo, é o valor médio dos quadrados dos desvios relativamente ao valor médio, dos dados que se obtêm quando se observa essa variável sobre todos os elementos da população, que assumimos finita.

##References
[1]Variance, wikipedia, https://en.wikipedia.org/wiki/Variance


