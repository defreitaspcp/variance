/*
Author : de Freitas, P.C.P
Description - Variance
References
[1]Variance, wikipedia, https://en.wikipedia.org/wiki/Variance
*/
#include<iostream>
using namespace std;
double mean(double data[], int length)
{
	double sum = 0;
	for (size_t i = 0; i < length; i++)
	{
		sum += data[i];
	}
	return sum / length;
}
double sampleVariance(double data[], int length)
{
	double sum = 0;
	for (size_t i = 0; i < length; i++)
	{
		sum += pow(data[i] - mean(data, length), 2);
	}
	return sum / length - 1;
}
double populationVariance(double data[], int length)
{
	double sum = 0;
	for (size_t i = 0; i < length; i++)
	{
		sum += pow(data[i] - mean(data, length), 2);
	}
	return sum / length;
}
int main()
{
	double CDAM20182[8] = { 1.02,0.99,0.84,1.06,1.02,1.02,0.99,0.99 };
	double NEAM20182[8] = { 7,8.25,8,8.75,6.5,2.25,7,4.5 };
	cout << "Sample Variance : " << sampleVariance(CDAM20182, 8) << endl;
	cout << "Population Variance : " << populationVariance(NEAM20182, 8) << endl;
	return 0;
}